var fileOpener = require('../index.js');
var should = require('chai').should();
var expect = require('chai').expect;

describe('File opening test', function(){
	var file = null;
	this.timeout(60000);
	before(function(done){
		
		fileOpener.openFile(__dirname + '/sample_spectra.mzXML', function(err, data){
			if(err){
				return done(err);
			}
			file = data;
			return done();
		});
		
	})
	
	it('check file structure', function(done){
		should.exist(file);
		file.should.be.an.array;
		
		var ms1 = file[0];
		
		//check if all spectra are there.
		file.length.should.eql(317);
		should.exist(ms1.peaks);
	    ms1.peaks.should.be.an('array');
		
		// check if all the peaks are there.
		ms1.peaks.length.should.eql(473);
		var peak = ms1.peaks[0];
		
		peak[0].should.be.a.Number;
		peak[1].should.be.a.Number;
		
		// check if the required values are there
		should.exist(ms1.maxValues);
		should.exist(ms1.maxValues.maxMZ);
		should.exist(ms1.maxValues.maxIntensity);
		
		should.exist(ms1.minValues);
		should.exist(ms1.minValues.minMZ);
		should.exist(ms1.minValues.minIntensity)
		
		return done();
		
	});
})