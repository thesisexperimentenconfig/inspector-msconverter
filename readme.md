# Usage

`openFile(path: String, callback: Function)`: tries to open the file located at `path` if successfull data is passed to the callback, if unsuccessfull an error is passed to the callback. The first argument of the callback is the error, the second one is data.

# Tests
After installing
    npm install
    npm install -g istanbul

run the command
    npm test
	
in terminal.