__author__ = 'Stefan'

# to read commandline arguments
import sys
import pyopenms

def load_experiment(filename):
    experiment = pyopenms.MSExperiment()
    filehandler = pyopenms.FileHandler()
    filehandler.loadExperiment(filename, experiment)
    return experiment

def save_spectra(exp, filename):
    file = open(filename, 'w')

    file.write("{\"experiment\":[")
    # exp is a MSExperiment object, we iterate over MSSpectrum objects
    for i,spectrum in enumerate(exp):
        if i > 0:
            file.write(",")
        file.write("{\"peaks\":[")
        maxMZ = 0
        maxIntensity = 0
        minMZ = float("inf")
        minIntensity = float("inf")

        # we sort by intensity first, true to sort descending
        spectrum.sortByIntensity(True)
        # we iterate over the Peak1D objects in the MSSpectrum object
        for j,peak in enumerate(spectrum):
            if j > 0:
                file.write(",")
            file.write("[" + str(peak.getMZ()) + "," + str(peak.getIntensity()) + "]")
            # we look for the maximum mz value
            if peak.getMZ() > maxMZ:
                maxMZ = peak.getMZ()
            # we look for the maximum intensity value
            if peak.getIntensity() > maxIntensity:
                maxIntensity = peak.getIntensity()
            # we look for the minimum intensity value
            if peak.getIntensity() < minIntensity:
                minIntensity = peak.getIntensity()
            # we look for the minimum intensity value
            if peak.getMZ() < minMZ:
                minMZ = peak.getMZ()

        file.write("], \"maxValues\":")
        file.write("{ \"maxMZ\":" + str(maxMZ) + ", \"maxIntensity\":" + str(maxIntensity) + "},")
        file.write("\"RT\":" + str(spectrum.getRT()) + ",")
        file.write("\"minValues\":")
        file.write("{ \"minMZ\":" + str(minMZ) + ", \"minIntensity\":" + str(minIntensity) + "}}")

    file.write("]}")
    file.close();

# load the MSExperiment
filename = sys.argv[1]
out_name = sys.argv[2]
experiment = load_experiment(filename)

#reset the output and write result
save_spectra(experiment, out_name)



