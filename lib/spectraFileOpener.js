var fs = require('fs');
var exec = require('child_process').exec;
var path = require('path');

module.exports = {

    /*
     * Proxy method to open files containing mass spectra and convert them to a more
     * convenient json format for javascript. The resulting json is passed
     * to the callback function (if one is provided).
     */
    openFile: function (filepath,  callback) {
        
        // The path to the python script
        var script = path.normalize(__dirname + '/generator.py ');
        var outputName = path.normalize(process.cwd()  + '/data.json');
        // executable string
        var exe = 'python ' + script +  ' ';
        
        // create child process
        var child = exec(exe + filepath + ' ' + outputName, function (err, stdout, stderr) {

            console.log(stdout);

            console.log('stderr: ' + stderr);

            if (err !== null) {
                console.log('exec error: ' + err);
                return callback(err);
                
            }

            var data = fs.readFileSync(outputName);
            var dat = JSON.parse(data);
            var experiment = dat.experiment;
            
            // call the callback if one is provided.
            typeof callback === 'function' && callback(null, experiment);


        });
    }
}